# Make sure that build/ is a directory before you run this script.
cp -r content/* build/
cd build
echo "" >> index.gmi
echo "## Blog" >> index.gmi
echo "" >> index.gmi
files=(blog/*.gmi)
for ((i=${#files[@]}-1; i>=0; i--)); do
	FILE=${files[$i]}
	NAME=${FILE##*_}
	NAME=${NAME%%.*}
	NAME=${NAME//-/ }
	DATE=${FILE%%_*}
	DATE=${DATE##*/}
	echo "=> $FILE $DATE: $NAME" >> index.gmi
	PUBLISHED="Published on $(date -d $DATE "+%B %-d, %Y")."
	sed -ie "0,/^#/a\\\n$PUBLISHED" $FILE
done
cd ..
