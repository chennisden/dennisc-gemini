# Gateway 14.1" Laptop

Recently I'd been casually looking through laptops and found the Gateway 14.1" Laptop. For $400 (the original price is $500, I'm pretty sure, but as of the time of writing it is on sale) the specs are quite amazing:

* i5 11th Gen (could be better, but is quite good)
* 16GB of RAM
* 512GB SSD

=> https://www.walmart.com/ip/Gateway-14-1-Ultra-Slim-Notebook-FHD-Intel-Core-i5-1135G7-Quad-Core-Iris-Xe-Graphics-16GB-RAM-512GB-SSD-Tuned-THX-Fingerprint-Scanner-1MP-Webcam-HDMI/982087215 Gateway 14.1" Laptop

When you consider that it has more RAM and storage than a MacBook Pro...

=> https://www.apple.com/macbook-pro-13/specs/ MacBook Pro

One caveat for the Gateway 14.1" though:

> Unfortunately the RAM on the Gateway 14.1" FHD Ultra Slim Notebook is built in and can not be upgraded. We sincerely apologize for the inconvenience, please kindly reach out to us through our other contact methods via chat on www.gatewayusa.com, by phone at 1-877-777-0649 or email us at support@gatewayusa.com. Our agents will assist you.

=> https://www.gatewayusa.com/GWTN141-5.html RAM is not upgradable

According to CNET the keyboard isn't backlight. To which I say: awesome, screw those battery-draining lights. (Also the fingerprint reader is bad, but who uses those anyways?)

=> https://www.cnet.com/tech/computing/gateway-14-1-inch-ultra-slim-notebook-2021-review-legen-dairy-budget-laptop-returns/ CNET Review

Anyway, I'm not getting a Gateway now because I'm going to stick with my Lenovo 710S Ideapad until either I start grad school or the RAM gives out (it's soldered on and I don't want to mess with it, but also RAM sticks rarely go bad). By then, I think the best laptop to buy will change. But if anyone out there needs a new laptop, I think this might be a good choice.
