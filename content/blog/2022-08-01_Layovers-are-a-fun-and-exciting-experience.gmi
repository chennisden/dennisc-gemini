# Layovers are a fun and exciting experience (or, sleep deprivation)

On my flight to Venice I experienced a fun and exciting 8 hour layover at the Heathrow airport. I fell asleep at the terminal, woke up, and immediately started questioning my own existence (hey man, I was sleepy). Here is what I wrote verbatim an hour after I woke up.

> As I am writing this my cognitive capacities have been severely diminished. I can still perform basic arithmetic in my headd, and my recall (which has traditionally been my strong point) has been retained. However, "complex" commands like "drop off your bag here if you've been told to check it" are difficult to comprehend.
> 
> So how did I get here? I'm going to omit any specific details and discuss this in general terms - the psychological changes (i.e. "the shit going on in my head") is the most interesting part of this story anyways.
> 
> In short, I am on a school trip going from our home airport to Way Far, and on our way over to Way Far we had a layover at Layover (which is also Way Far from Home, but fairly close to Way Far). Originally our layover was a sane 2 hours or something, but due to an array of flight delays, we eneded up staying for like 9 hours, and we left late in the afternoon, dear God it's been more than 24 hours since I've slept in a proper bed.
> 
> I was still "sober" for the majority of the layover, but after getting dinner (does it even count as dinner?) I decided to lie down on a chair to rest a little. I ended up passing out cold, and I still have no idea who woke me up before we walked from Terminal Gate X to Terminal Gate Y (because at Heathrow they announce your fucking boarding gate 10 minutes before you board. Thanks, assholes.) I felt as if I had no idea what was going on and that everything I experience prior to a day ago was just a long convoluted dream. I was pretty much on autopilot after I woke up.
> 
> The biggest effect of my sleep deprivation so far is my detachment from whatever's going on ("am I even real", "how did I get here", etc). There was no room in my head for trivial concerns like college applications or whatever.
> 
> Even though my cognitive processing power has vanished entirely, my personality largely remained the same. What's most interesting is that even though I must've known what I was going to say on some level, wit comes as a completely surprise to me. Take this sarcastic title for instance. I knew somewhere deep in my soul that this would be the perfect title, but unlike usual I have no idea what led me to it.[^1] I'm sure this only exacerbated the detachment I felt. At the same time, I could barely process what other people were doing and saying.
> 
> [^1]: This makes me feel as if personality is a largely subconscious thing. Granted, my brain's operating pretty much as if I was shitfaced drunk, not that I would even know what being drunk is like.
> 
> I'd close this post with a good conclusion but my brain has literally has three functional cells right now. So this will have to do.

The footnote in the writing was because I initially planned on submitting this to my website, but after 30 seconds of thinking after I woke up not sleep deprived later, I decided "yeah, no, Gemlog it is."

I still have no idea how I had enough brainpower to write that on an hour of sleep.
