# Puzzle Musings

I've had a lot of miscellaneous thoughts about puzzles lately and I want to write them down somewhere.

## Theory vs. Discovery

Obviously different puzzles have different audiences, and therefore will make different assumptions about what knowledge is "standard". (A sudoku for grannies is probably not going to include X-Wings.) However, for some genres, there is a clear barrier of theory between beginner level puzzles and intermediate level puzzles.

Theory-heavy genres include Slitherlink and U-Bahn. As a rule of thumb, the more theory-heavy a genre is, the harder it is to derive its ideas from scratch. Generally you will be reasoning in more abstract, higher-level building blocks of logic, like "oh there are two diagonally connected 3s" and not "because of -proof by contradiction- these two diagonally connected 3s must resolve like this". Some puzzles will be chock-full of concepts that you may not know but are expected to.

Variant sudoku is sort of an exception. You will be expected to know what an X-Wing is, and obviously pointing pairs and triples or whatever, but nothing super technical. In fact, that's why I like it: I don't need to read a 50-page PDF to realize that German Whispers don't contain 5s and alternate between high and low. For the same reason, I usually steer clear of most loop-based genres. There's too many precise arguments and things to keep track of, especially when you don't know what you're expected to know. (For some reason, shading and number placement puzzles seem less theory-focused than loops. I don't know why.)

The puzzles I set are typically very light on theory. There are few global arguments, rather I include clever local arguments. (Notable exception: Alphabet Soup. It's the kind of puzzle I wouldn't solve myself if I encountered it in the wild, and it's kinda weird, which is why I'm so fond of it.)

### Some theory is good

However, some theory is definitely good. Imagine if we lived in a world where X-Wings didn't exist. Then we couldn't have puzzles like Quad Knights Out of Order.

=> https://logic-masters.de/Raetselportal/Raetsel/zeigen.php?id=000EKI Quadruple Knights Out of Order

It's fun to use The Secret (the digits 1-9 sum to 45), or to use complementary counting for big Killer Cages (e.g. 6-cell 37 cages), or to do coloring puzzles. You wouldn't really be able to do a coloring puzzle without knowing what coloring was, and I'm glad that everyone is expected to be able to do coloring (especially for Anti-Knight puzzles).

Theory is a good thing and I'm glad there are people pushing the frontier of what is possible, so the rest of us can get our cool techniques without having to worry about Death Blossoms. I dunno, I guess it's just not for me though.

## Fog of War

To say Fog of War is popular would be quite an understatement. I have mixed feelings on the genre.

Some puzzles actually utilize Fog of War to make you deduce where cages or arrows go, e.g. Chameleon's original FoW. But with many others I have to wonder, "This is a great puzzle, but why did you even bother adding in FoW?" I won't actively avoid FoW puzzles, but at the same time, I don't find it something I actively seek out either (unlike Fillomino).

In some sense, I think Fog is a bastardization of what a puzzle really is. The beauty of a logic puzzle is that you are given some rules and a specific setup, and you have to figure out how they interact. Crucially, everything you need is already in the grid; you just need to figure out how it all interacts. The solution isn't "hidden" --- in theory, you can brute force all the possibilities --- but it's about finding where the puzzle is most susceptible to pressure. It's like seeing a mountain and being asked how a river would travel down it. The information is all there, now you just have to discover the path of least resistance. Fog on the other hand is more akin to a video game puzzle or an escape room, where deducing one thing leads to a revelation that helps you deduce another thing, ad nauseum.

Not that this is necessarily a bad thing: escape rooms and video game puzzles are great. But they are totally different from logic puzzles.

On a more pragmatic note, Fog is often used as a substitute for proper telegraphing. There are many Fog puzzles that have no unique fog-based deductions, and the fog is just there to make sure you complete the puzzle in the right order. Or sometimes it's just completely pointless. With more careful setting the puzzle wouldn't break like that, fog or no fog.

Also, in a Fog puzzle, you're not quite sure what information is supposed to be available to you, and a lot of the information that is available is very janky (e.g. based on software implementation differences). Like, "Oh I can see the line kinda peeking out this diagonal" and "The cage turns here 3 pixels away from the border of the fog so I know the cage doesn't go here" both feel very unsure and unsatisfying. You don't know if you made the right deduction, and even if you did it's not very fun.

I dunno. I don't love Fog. My 2c.

## Popularization of Sudoku, and the ambiguity of "puzzle"

Once upon a time only the hardcore Nikoli fanboys knew what puzzles were. They knew their Choco-Bananas and Kakuros and their Nurikabes too. Now is a different era. Everyone knows what a puzzle is, for some definition of puzzle.

Self introductions are hard. On the one hand you want to be relatable, so you pick some interests and point out likes/dislikes everyone knows/agrees with. OK, so you like anime and cats --- so does the rest of the entire human population. So on the other hand, you want to pick something distinctive. For me that interest would be puzzles.

But imagine how hard it is to tell people "hey I like puzzles" and then spend an eternity explaining what you actually meant. "Puzzle" is a word that means something to different to everyone. It's literally a nothing burger at this point, the word doesn't mean anything anymore because it took up so many different meanings. The word "puzzle" is probably more versatile than the f-word. Some examples:

- Jigsaw puzzles
- Puzzle hunts (e.g. MIT Puzzle Hunt)
- Crosswords
- Riddles (e.g. crossing the bridge, lamb-sheep-wolf, 100 green eyed people)
- Paradoxes (e.g. unexpected execution, when does a heap of sand stop heaping)
- ...and of course, pencil puzzles.

So on the one hand you can say, "I like setting puzzles", and everyone will go "oh that's pretty cool" and they'll *think* they know what you mean but they'll have no idea what you mean and you won't be able to follow up with the rare person who also does logic puzzles. Or you can say, "I set Nikoli-style pencil puzzles" and everyone will go, "English please I don't know any of the words you just said".

And even if you magically communicate that you set puzzles like Sudoku (pro tip: say "I set Sudoku puzzles" instead of "I set puzzles"), that still means different things to people. Inevitably some moron will ask "why don't you let the computer generate the puzzle for you". I know there's no bad intentions and it's just curiosity, and I'm also the kind of person who's super blunt so other people being blunt doesn't really affect me, but it feels like I'm going in the Louvre and looking at Picassos and everyone else is like "wow art is so cool right. I love AI generated art it just looks so pretty". People don't even know what a *break-in* is, or that puzzles can be constructed with *intent*, because all NYT Sudoku does is have you spot the naked single and place it.

Also, even if you say "I set sudoku", you'll still elicit a "wtf" reaction if you're deep enough in the rabbit hole. I used to say this but then people got curious and I had to show them some cursed abomination that barely even resembled sudoku. Some days even I think "what the actual heck is a Chaos Construction".

The worst thing is I'm only slightly peeved by it, because it's not a matter of life and death, it's just putting numbers in boxes. So I don't talk a lot about puzzles because that is an acceptable solution, and to make up for it I write about puzzles on my blog.

In an absolute sense I'm definitely "the puzzle guy". I do talk about puzzles with moderate frequency. But with the exception of a few people online, I don't really say anything super substantive and I don't go very in depth. Compared to what I'd be like if I was given free reign to talk about puzzles, I think I behave pretty differently.

Why is the only interesting thing about me so hard to explain. Goddamnit

(Caveat: Thank god for CTC, it's a really good proxy for whether people actually care about logic puzzles or not. You can just say "I watch Cracking the Cryptic" and skip past half the bullshit, and all you have to do is figure out whether they're some poser who only does variant sudoku and has never heard of a pencil puzzle before. Wait crap, that's me.)
