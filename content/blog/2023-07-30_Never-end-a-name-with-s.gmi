# Never end a name with 's'

The title of my puzzle website is "Rey's Puzzles".

=> puzz.dennisc.net My puzzle website

Rey is my middle name. There's a couple of reasons I've started going by my middle name more often, which mostly boil down to "I like it more". But until I made a separate puzzle website, I'd never used it in any of my branding.

Well, not this time. The reason was because Dennis' and Dennis's both looked really silly, so I just went with Rey. So here's a pro life tip for everyone: don't name your kid something that ends with s, it makes using the posessive so much harder. Thanks.
